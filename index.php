<?php
if ( file_exists(dirname(__FILE__).'/vendor/autoload.php') ) {
    require_once dirname(__FILE__).'/vendor/autoload.php';
}
use PhpCfdi\SatWsDescargaMasiva\Shared\ServiceEndpoints;
use PhpCfdi\SatWsDescargaMasiva\RequestBuilder\FielRequestBuilder\FielRequestBuilder;
use PhpCfdi\SatWsDescargaMasiva\RequestBuilder\FielRequestBuilder\Fiel;

use PhpCfdi\SatWsDescargaMasiva\Service;
use PhpCfdi\SatWsDescargaMasiva\WebClient\GuzzleWebClient;

// Variables principales
$RFC = "tj"; // id del rfc a descargar tj,eypo,parque,meqsa,old,noble
$FechaI = "2022-05-01 00:00:00";
$FechaF = "2022-05-27 23:59:59"; 
$accion = "Issued"; // Isueed, Received

//Conexion db
$servername = "SEYPO";
$conectinfo = array (
    "Database"=>"EYPO", 
    "UID"=>"sa",
    "PWD"=>"3yP02020",
    "CharacterSet" => "UTF-8",
);
$conn = sqlsrv_connect($servername, $conectinfo);

//credenciales
switch ($RFC) {
    case 'tj':
        // Creación de la FIEL, puede leer archivos DER (como los envía el SAT) o PEM (convertidos con openssl)
        $fiel = Fiel::create(file_get_contents('00001000000509805247.cer'), file_get_contents('Claveprivada_FIEL_TLE041108QP8_20211109_093725.key'), 'Tl010730');
        break;

    case 'eypo':
        // Creación de la FIEL, puede leer archivos DER (como los envía el SAT) o PEM (convertidos con openssl)
        $fiel = Fiel::create(file_get_contents('00001000000508067642.cer'), file_get_contents('Claveprivada_FIEL_EPO020828E5A_20210706_110328.key'), 'Ep010730');
        break;

    case 'parque':
        // Creación de la FIEL, puede leer archivos DER (como los envía el SAT) o PEM (convertidos con openssl)
        $fiel = Fiel::create(file_get_contents('mpa140527ub9.cer'), file_get_contents('Claveprivada_FIEL_MPA140527UB9_20180801_091438.key'), 'Mp010730');
        break;

    case 'meqsa':
        // Creación de la FIEL, puede leer archivos DER (como los envía el SAT) o PEM (convertidos con openssl)
        $fiel = Fiel::create(file_get_contents('00001000000504234899.cer'), file_get_contents('Claveprivada_FIEL_MQU090428812_20200618_100605.key'), 'Mq010730');
        break;

    case 'old':
        // Creación de la FIEL, puede leer archivos DER (como los envía el SAT) o PEM (convertidos con openssl)
        $fiel = Fiel::create(file_get_contents('oco080911bw8.cer'), file_get_contents('Claveprivada_FIEL_OCO080911BW8_20180116_145517.key'), 'Oc010730');
        break;

    case 'noble':
        // Creación de la FIEL, puede leer archivos DER (como los envía el SAT) o PEM (convertidos con openssl)
        $fiel = Fiel::create(file_get_contents('naj070821rx4.cer'), file_get_contents('Claveprivada_FIEL_NAJ070821RX4_20180118_150109.key'), 'Na010730');
        break;

    default:
        # code...
        break;
}

// verificar que la FIEL sea válida (no sea CSD y sea vigente acorde a la fecha del sistema)
if (! $fiel->isValid()) {
    return;
}

// creación del web client basado en Guzzle que implementa WebClientInterface
// para usarlo necesitas instalar guzzlehttp/guzzle pues no es una dependencia directa
$webClient = new GuzzleWebClient();

// creación del objeto encargado de crear las solicitudes firmadas usando una FIEL
$requestBuilder = new FielRequestBuilder($fiel);

// Creación del servicio
$service = new Service($requestBuilder, $webClient);
// Cliente para consumir los servicios de CFDI de Retenciones
// Existen dos tipos de Comprobantes Fiscales Digitales, los regulares (ingresos, egresos, traslados, nóminas y pagos), y los CFDI de retenciones e información de pagos (retenciones).

// Puede utilizar esta librería para consumir los CFDI de Retenciones. Para lograrlo construya el servicio con la especificación de ServiceEndpoints::retenciones().

use PhpCfdi\SatWsDescargaMasiva\RequestBuilder\RequestBuilderInterface;


/**
 * @var GuzzleWebClient $webClient
 * @var RequestBuilderInterface $requestBuilder
 */
// Creación del servicio de retenciones
//$service = new Service($requestBuilder, $webClient, null, ServiceEndpoints::retenciones());


use PhpCfdi\SatWsDescargaMasiva\Services\Query\QueryParameters;
use PhpCfdi\SatWsDescargaMasiva\Shared\DateTimePeriod;
use PhpCfdi\SatWsDescargaMasiva\Shared\DownloadType;
use PhpCfdi\SatWsDescargaMasiva\Shared\RequestType;

/**
 * El servicio ya existe
 * @var Service $service
 */

// Explicación de la consulta:
// - Del 13/ene/2019 00:00:00 al 13/ene/2019 23:59:59 (inclusive)
// - Todos los emitidos por el dueño de la FIEL
// - Solicitando la información de Metadata
// - Filtrando los CFDI emitidos para RFC MAG041126GT8
if($accion == 'Issued'){
    $request = QueryParameters::create(
        DateTimePeriod::createFromValues($FechaI, $FechaF),
        DownloadType::issued(),
        RequestType::Metadata()
        //,
    //  'ADD150624GD8'
    );
}else{
    $request = QueryParameters::create(
        DateTimePeriod::createFromValues($FechaI, $FechaF),
        DownloadType::received(),
        RequestType::Metadata()
        //,
    //  'ADD150624GD8'
    );
}

// presentar la consulta

$query = $service->query($request);

// verificar que el proceso de consulta fue correcto
if (! $query->getStatus()->isAccepted()) {
    echo "Fallo al presentar la consulta: {$query->getStatus()->getMessage()}";
    return;
}

// el identificador de la consulta está en $query->getRequestId()
// echo "Se generó la solicitud {$query->getRequestId()}", PHP_EOL;


$requestId = $query->getRequestId();

/**
 * @var Service $service
 
 */

// consultar el servicio de verificación
$verify = $service->verify($requestId);
echo $requestId;
ini_set('max_execution_time', '5000'); 
set_time_limit(2000);
sleep(5);

// revisar que el proceso de verificación fue correcto
if (! $verify->getstatus()->isaccepted()) {
    echo "fallo al verificar la consulta {$requestid}: {$verify->getstatus()->getmessage()}";
    return;
}

// revisar que la consulta no haya sido rechazada
if (! $verify->getcoderequest()->isaccepted()) {
    echo "la solicitud {$requestid} fue rechazada: {$verify->getcoderequest()->getmessage()}", php_eol;
    return;
}

sleep(60);

// revisar el progreso de la generación de los paquetes
$statusrequest = $verify->getstatusrequest();
if ($statusrequest->isexpired() || $statusrequest->isfailure() || $statusrequest->isrejected()) {
    echo "la solicitud {$requestid} no se puede completar", php_eol;
    return;
}

if ($statusrequest->isinprogress() || $statusrequest->isaccepted()) {
    echo "la solicitud {$requestid} se está procesando", php_eol;
	
    return;
}
if ($statusrequest->isfinished()) {
    echo "la solicitud {$requestid} está lista", php_eol;
}

echo "Se encontraron {$verify->countPackages()} paquetes", PHP_EOL;
foreach ($verify->getPackagesIds() as $packageId) {
	//echo ($packageId);
   // echo " > {$packageId}", PHP_EOL;
	$download = $service->download($packageId);
	echo 'hola';
    if (! $download->getStatus()->isAccepted()) {
        echo "El paquete {$packageId} no se ha podido descargar: {$download->getStatus()->getMessage()}", PHP_EOL;
        continue;
    }
	
    $zipfile = "$packageId.zip";
    file_put_contents($zipfile, $download->getPackageContent());
    echo "El paquete {$packageId} se ha almacenado", PHP_EOL;
	//$packagesIds = $packageId;
}

// ingreso de la respuesta
use PhpCfdi\SatWsDescargaMasiva\PackageReader\Exceptions\OpenZipFileException;
use PhpCfdi\SatWsDescargaMasiva\PackageReader\MetadataPackageReader;

/**
 * @var string $zipfile contiene la ruta al archivo de paquete de Metadata
 */

// abrir el archivo de Metadata
//echo 'hola';
try {
    $metadataReader = MetadataPackageReader::createFromFile($zipfile);
} catch (OpenZipFileException $exception) {
    echo $exception->getMessage(), PHP_EOL;
    return;
}

// leer todos los registros de metadata dentro de todos los archivos del archivo ZIP
foreach ($metadataReader->metadata() as $uuid => $metadata) {
    echo $metadata->uuid, ': ', $metadata->fechaEmision, PHP_EOL;

    //inicio del ingreso 
    $sql = "INSERT INTO [Descargas].[dbo].[Metadata] ([UUID],[RfcEmisor],[NombreEmisor],[RfcReceptor],[FechaEmision],[RfcPac],[Total],[TipoCFDI] ,[Estatus],[FechaCancelacion],[Descargado])";
    $values = compose_value($metadata,$RFC);
    # sql
    $sqlinsert = $sql + $values;
    // Search for a meta already in the database
    $sqlsearch = "SELECT Count(*) as cuenta FROM [Descargas].[dbo].[Metadata] where UUID = '" . $exam->UUID . "' and Descargado = '" + $RFC + "'";
    //busqueda de search
    $consultasql = sqlsrv_query($conn,$sqlsearch);
    // Con fetchall traemos todas las filas
    while ($Row = sqlsrv_fetch_array($consultasql)) {
        $cuenta = $Row['cuenta'];
    }

    // Revisar e insertar o no
    if( $cuenta == 1){
        // No hacer nada
        echo("No ingresado");
    }else{
        sqlsrv_query($conn, $sqlinsert);
        echo("insertado:" . $exam->UUID . "\n");
    }
}






function compose_value($exam,$rfc){
    //compose the values part in the insert method
    if($exam->Estatus == '1'){ //if de estatus
        if($exam->EfectoComprobante != 'P'){
            echo("activo");
            $values = "VALUES ('" . $exam->UUID . "','" . $exam->rfcemisor . "','" . $exam->NombreEmisor . "','" . $exam->RfcReceptor . "','" . $exam->Fecha . "','" . $exam->RfcPac . "','" . $exam->Total . "','" . $exam->EfectoComprobante . "','Activo','','" . $RFC . "')";
        }else{
            echo("activo");
            $values = "VALUES ('" . $exam->UUID . "','" . $exam->rfcemisor . "','" . $exam->NombreEmisor . "','" . $exam->RfcReceptor . "','" . $exam->Fecha . "','" . $exam->RfcPac . "','','" . $exam->EfectoComprobante . "','Activo','','" . $RFC . "')" ;
        }
    }else{ //else estatus
        if($exam->EfectoComprobante != 'P'){
            echo("cancelado");
            $values = "VALUES ('" . $exam->UUID . "','" . $exam->rfcemisor . "','" . $exam->NombreEmisor . "','" . $exam->RfcReceptor . "','" . $exam->Fecha . "','"  . $exam->RfcPac . "','" . $exam->Total . "','" . $exam->EfectoComprobante ."','Cancelado','" . $exam->FechaCancelacion . "','" . $RFC . "')" ;
        }else{
            echo("cancelado");
            $values = "VALUES ('" . $exam->UUID . "','" . $exam->rfcemisor . "','" . $exam->NombreEmisor . "','" . $exam->RfcReceptor . "','" . $exam->Fecha . "','" . $exam->RfcPac . "',' ','" . $exam->EfectoComprobante ."','Cancelado','" . $exam->FechaCancelacion . "','" . $RFC . "')" ;
        }
    }
    return $values;
}


